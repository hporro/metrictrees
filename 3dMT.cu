#include <cuda.h>
#include <random>
#include <MetricTree.cuh>
#include <iostream>

template<class T>
T* initialize_val_on_gpu(int n, T* a){
    T* d_res;
    cudaMalloc(&d_res,(int)(n*sizeof(T)));
    cudaMemcpy(d_res, a, n*sizeof(T), cudaMemcpyHostToDevice);
    return d_res;
}

int main(){
    float square_side = 200;
    
    std::default_random_engine gen;
    std::uniform_real_distribution<float> uniform_dist(-square_side/2,square_side/2);

    uint n = 100;

    MT3DNode *nodes;
    cudaMalloc(&nodes,(int)(n*sizeof(MT3DNode)));

    int h_root[] = {0};
    int *root = initialize_val_on_gpu(1,h_root);

    float3 *h_pos = new float3[n];
    for(int i=0;i<n;i++){
        h_pos[i].x = uniform_dist(gen);
        h_pos[i].y = uniform_dist(gen);
        h_pos[i].z = uniform_dist(gen);
    }
    float3 *pos = initialize_val_on_gpu(n,h_pos);

    float *h_radiuses = new float[n];
    for(int i=0;i<n;i++){
        h_radiuses[i] = 1;
    }
    float *radiuses = initialize_val_on_gpu(n,h_radiuses);

    float delta = square_side;

    int h_created_node_count[] = {0};
    int *created_node_count = initialize_val_on_gpu(1,h_created_node_count);

    d_build3DMT<<<n,1>>>(n, nodes, root, pos, radiuses, delta, created_node_count);

    MT3DNode *h_nodes = new MT3DNode[n];
    cudaMemcpy(h_nodes, nodes, n*sizeof(MT3DNode), cudaMemcpyDeviceToHost);

    for(int i=0;i<n;i++){
        std::cout << "(" << h_nodes[i].particle_id << ": " << h_nodes[i].children.x << ", " << h_nodes[i].children.y << ", " << h_nodes[i].children.z << ")\n";
    }std::cout << std::endl;

    return 0;
}