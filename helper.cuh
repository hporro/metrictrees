#pragma once

template<class T>
T* initialize_val_on_gpu(int n, T* d_a){
    T* d_res;
    cudaMalloc(&d_res,(int)(n*sizeof(T)));
    cudaMemcpy(d_res, d_a, n*sizeof(T), cudaMemcpyHostToDevice);
    return d_res;
}

__device__ static float atomicMax(float* address, float val){
    int* address_as_i = (int*) address;
    int old = *address_as_i, assumed;
    do {
        assumed = old;
        old = ::atomicCAS(address_as_i, assumed,
            __float_as_int(::fmaxf(val, __int_as_float(assumed))));
    } while (assumed != old);
    return __int_as_float(old);
}
