#pragma once

struct MT3DNode {
    float3 pos;
    float radius;
    float delta;
    int particle_id;
    //float2 wedges;
    int4 children;
};