#pragma once

#include "helper.cuh"

typedef unsigned int uint;

__global__ void d_build3DMT(int no, MT3DNode *nodes, int *root, float4 *oldPos, float delta, int *created_node_count);
__global__ void d_query3DMT(int no, MT3DNode *nodes, int *root, float4 *oldPos, float4 *oldVel, float4 *newVel);

__global__ void d_build3DMT(int no, MT3DNode *nodes, int *root, float4 *oldPos, float delta, int *created_node_count){
    const int i = threadIdx.x + blockIdx.x*blockDim.x;

    if( i>= no ) return;

    MT3DNode n;

    int node_array_index = atomicAdd(created_node_count,1);

    float3 pos = make_float3(oldPos[i]);
    n.delta = delta; // node radius
    n.pos = pos; // particle position
    n.radius = params.particleRadius; // particle radius
    n.particle_id = i; // particle id
    //n.wedges = float2{0.0,0.0}; // max in_edge in .x and out_edge in .y
    n.children = int4{-1,-1,-1,-1}; // three child nodes. child[0] is out, child[1] is on and child[2] is in.

    nodes[node_array_index] = n;

    int current = atomicCAS( root, -1, node_array_index );

    while( current != -1 ) {

        MT3DNode father = nodes[current];

        int side = 0;

        float d = length(father.pos - pos);

        // 1 -> outside the sphere
        if( d >= ( father.delta + params.particleRadius ) ) {
            side = 1;
        // -1 -> inside the sphere
        } else if( d <= ( father.delta - params.particleRadius ) ) {
            side = -1;
        }

        if(side == -1)
            nodes[node_array_index].delta *= 0.5;

        if( side == 1 ) {
            current = atomicCAS( &nodes[current].children.x, -1, node_array_index );
        } else if( side == -1 ) {
            current = atomicCAS( &nodes[current].children.z, -1, node_array_index );
        } else {
            //float in_wedge = father.delta - ( d - params.particleRadius );
            //float out_wedge = ( d + params.particleRadius ) - father.delta;
            //float wedge = max( ( in_wedge ), ( out_wedge ) );
            //atomicMax( &nodes[current].wedges.x, in_wedge);
            //atomicMax( &nodes[current].wedges.y, out_wedge);
            current = atomicCAS( &nodes[current].children.y, -1, node_array_index );
        }

    }
}

__global__ void d_query3DMT(int no, MT3DNode *nodes, int *root, float4 *oldPos, float4 *oldVel) {
    const uint i = threadIdx.x + blockIdx.x*blockDim.x;

    if(i>=no)return;

    int index = *root;
    float3 p = make_float3(oldPos[i]);
    float3 vel = make_float3(oldVel[i]);
    
    int stack[32];
    int stack_size = 0;
    stack[stack_size++] = -1;

    float3 force = make_float3(0.0f);
    
    do {
        const MT3DNode node = nodes[index];
        
        const float d = length( p - node.pos );
        const float delta = node.delta;
        
        if( d <= 2*params.particleRadius ) {
			int particle_id = int(node.particle_id);
            // do operation between particles indexed at "particle_id" and "i"
            if(i!=particle_id){
                //printf("particles %d-%d\n",i,particle_id);
                force += collideSpheres(p, make_float3(oldPos[particle_id]), vel, make_float3(oldVel[particle_id]), params.particleRadius, params.particleRadius, params.attraction);
            }
        }
        
        //if((node.children.y > 0) && 
        //   (d > (delta - node.wedges.x))
        //   &&( d < (delta + node.wedges.y))) {
        if(node.children.y > 0){
            stack[stack_size++] = node.children.y;
        }
        
        // outside
        //if( delta_distance > 0.0 ) {
        if(d >= (delta + params.particleRadius) ) {
            index = (node.children.x!=-1) ? node.children.x : stack[--stack_size];
        }
        // inside
        else if( d <= ( delta - params.particleRadius ) ) {
        //else {
            index = (node.children.z!=-1) ? node.children.z : stack[--stack_size];
        }
        // overlaped
        else {
            if(node.children.x!=-1) stack[stack_size++] = node.children.x;
            if(node.children.z!=-1) stack[stack_size++] = node.children.z;
            index = stack[--stack_size];
        }
    } while( index > -1 );

    // collide with cursor sphere
    force += collideSpheres(p, params.colliderPos, vel, make_float3(0.0f, 0.0f, 0.0f), params.particleRadius, params.colliderRadius, 0.0f);

    oldVel[i] = make_float4(vel + force, 0.0f);
}

__global__ void init_buffers_mt_kernel(int *d_root, int *d_created_nodes_count){
    d_root[0]=-1;
    d_created_nodes_count[0]=0;
}