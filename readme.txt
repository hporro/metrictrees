Only tested in Windows and linux
For windows, use VS2019 to compile, while for Linux use the makefile.

You can press the key `s` in the keyboard to change the acceleration data structure between metric trees and a grid.
You can provide the executable `particles` the option `n=x` with `x` equal to a number to execute the code with a different number of particles.
You can provide the executable `particles` the option `benchmark` to run a benchmark comparing both data structures. In my laptop the grid runs around 5x times faster.
You can do the benchmarks on other cuda capable gpus using the `device=x` option.

# Cuda Readme below

Sample: particles
Minimum spec: SM 3.5

This sample uses CUDA to simulate and visualize a large set of particles and their physical interaction.  Adding "-particles=<N>" to the command line will allow users to set # of particles for simulation.  This example implements a uniform grid data structure using either atomic operations or a fast radix sort from the Thrust library

Key concepts:
Graphics Interop
Data Parallel Algorithms
Physically-Based Simulation
Performance Strategies

